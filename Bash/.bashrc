# Language Japanese
export LANG=ja_JP.UTF-8
export LESSCHARSET=utf-8

# Alias ls
alias ls='ls -G'
alias la='ls -a'
alias ll='ls -al'

# Alias ShellScript
alias hidden='~/dotfiles/ShellScript/hidden.sh'

# Alias Vim
alias vi='vim'

# Color Prompt (Raspberry Pi like)
export PS1="\e[01;32m\]\u@\h \e[00m\]\e[01;34m\]\W \$ \e[00m\]"

# Color ls
export CLICOLOR=1
export LSCOLORS=DxGxcxdxCxegedabagacad

# Path Homebrew
export PATH=/usr/local/sbin:$PATH
export PATH=/usr/local/bin:$PATH

# Path nodebrew
# export PATH=$HOME/.nodebrew/current/bin:$PATH

# Path Android SDK Tools
# export PATH=$PATH:/Users/doublehorn93/android_dev/sdk/platform-tools
